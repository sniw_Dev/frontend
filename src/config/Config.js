/** @format */

const APIURL = 'http://localhost:5000/api';
const Token_Header = () => {
  const token = localStorage.getItem('token');
  if (token) {
    return {
      headers: {
        'x-access-token': `${token}`,
      },
    };
  }
};
module.exports = {
  APIURL,
  Token_Header,
};
