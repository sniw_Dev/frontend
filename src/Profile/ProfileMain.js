/** @format */

import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Modal } from 'react-bootstrap';
import Button from '@mui/material/Button';
import { useContext } from 'react';
import { UserContext } from '../hooks/UserContext';
import { updateUser } from '../api/users/UserService';
import useAuth from './../hooks/useAuth';
import { Getuser } from '../api/users/UserService';
import './ProfileMain.css';
import { useFormik } from 'formik';

//{"_id":"6250dd7c19b6464327b0baf2","nom":"mansouri","prenom":"ghassen","email":"nizar@wealzy.com","password":"$2b$10$A8mxcA/xVhB2AQVphxpwZObxcUPZIafqdRCIbvgGMtH3WjhF4rSBi","address":"6 rue marseille el mourouj 4","codepostal":"BEN AROUS","image":"1649466747825_rayen.png","genre":"Homme","__v":0}
const ProfileMain = (props) => {
  const { setUserContext } = useAuth();

  const { user } = useContext(UserContext);
  const [userinit, setuserinit] = useState('');
  const [userform, setuser] = useState(user);
  const [update, setupdate] = useState(false);
  const [email, setemail] = useState('');
  const [nom, setnom] = useState('');
  const [prenom, setprenom] = useState('');
  const [password, setpassword] = useState('');
  const [address, setAddress] = useState('');
  const [code_postal, setcode_postal] = useState('');
  const [genre, setgenre] = useState('');

  const initialValues = {
    email: '',
    password: '',
    nom: '',
    prenom: '',
    address: '',
    code_postal: '',
    image: '',
    genre: 'H',
  };

  const onSubmit = async (values) => {
    const usertoupdate = {
      nom: values.nom,
      prenom: values.prenom,
      email: values.email,
      password: values.password,
      address: values.address,
      code_postal: values.code_postal,
      genre: values.genre,
    };
    updateUser(usertoupdate)
      .then((res) => {
        RefreshUser();
        console.log(res);
        setUserContext(res.data);
        props.handleupdate();
      })
      .catch((err) => {});
  };

  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = 'obligatoire';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = 'Invalid email address';
    }
    if (!values.password) {
      errors.password = 'obligatoire';
    } else if (
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/i.test(
        values.password
      )
    ) {
      errors.password =
        'password doit contenir au moins 8 caractères dont au moins une majuscule, une minuscule, un chiffre et un caractère spécial';
    }

    if (!values.nom) {
      errors.nom = 'obligatoire';
    } else if (!/^[a-zA-Z]+$/i.test(values.nom)) {
      errors.nom = 'nom doit contenir que des lettres';
    }
    if (!values.prenom) {
      errors.prenom = 'obligatoire';
    } else if (!/^[a-zA-Z]+$/i.test(values.prenom)) {
      errors.prenom = 'prenom doit etre lettres seulement';
    }
    if (!values.address) {
      errors.address = 'obligatoire';
    }
    if (!values.code_postal) {
      errors.code_postal = 'Required';
    } else if (!/^[0-9]{5}$/i.test(values.code_postal)) {
      errors.code_postal = 'code postal doit etre un nombre de 5 chiffres';
    }

    return errors;
  };
  const formik = useFormik({
    enableReinitialize: true,

    initialValues,
    onSubmit,
    validate,
  });

  useEffect(() => {
    Getuser().then((res) => {
      console.log(res.data);
      console.log('usssssssssseEffect');
      setuserinit(res.data);
    });
  }, []);

  useEffect(() => {
    console.log(userinit);
  }, [userinit]);

  const RefreshUser = () => {
    Getuser().then((res) => {
      console.log(res.data);
      setuserinit(res.data);
    });
  };

  useEffect(() => {
    console.log(user, 'userProfileCard');
    if (user) {
      setuser(user);
      formik.setFieldValue('email', user.email);
      formik.setFieldValue('nom', user.nom);
      formik.setFieldValue('prenom', user.prenom);
      formik.setFieldValue('address', user.address);
      formik.setFieldValue('code_postal', user.codepostal);
      formik.setFieldValue('genre', user.genre);
    }
  }, [user, props.showupdate]);

  console.log(formik.values);
  console.log(formik.errors);

  useEffect(() => {
    setupdate(props.showupdate);
  }, [props.showupdate]);
  return (
    <div className='Container_Main'>
      <h2>Detail</h2>
      {update === false ? (
        <div className='Container_Detail'>
          <table>
            <tr>
              <td>Nom :</td>
              <td>{userinit.nom}</td>
            </tr>
            <tr>
              <td>Prenom :</td>
              <td>{userinit.prenom}</td>
            </tr>
            <tr>
              <td>Email :</td>
              <td>{userinit.email}</td>
            </tr>
            <tr>
              <td>address :</td>
              <td>{userinit.address}</td>
            </tr>
            <tr>
              <td>Code postale :</td>
              <td>{userinit.codepostal}</td>
            </tr>
            <tr>
              <td>Genre :</td>
              <td>{userinit.genre}</td>
            </tr>
          </table>
        </div>
      ) : (
        <div>
          <Form onSubmit={formik.handleSubmit}>
            <Row className='mb-3'>
              <Form.Group as={Col} controlId='nom'>
                <Form.Label>Nom:</Form.Label>
                <Form.Control
                  value={formik.values.nom}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.nom && formik.errors.nom ? (
                  <div>
                    <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                      {formik.errors.nom}
                    </Form.Label>
                  </div>
                ) : null}
              </Form.Group>
              <Form.Group as={Col} controlId='prenom'>
                <Form.Label>Prénom :</Form.Label>
                <Form.Control
                  value={formik.values.prenom}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />{' '}
                {formik.touched.prenom && formik.errors.prenom ? (
                  <div>
                    <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                      {formik.errors.prenom}
                    </Form.Label>
                  </div>
                ) : null}
              </Form.Group>
            </Row>

            <Row className='mb-3'>
              <Form.Group as={Col} controlId='email'>
                <Form.Label>Email :</Form.Label>
                <Form.Control
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  type='Email'
                  placeholder='XXX@EXEMPLE.com'
                  onBlur={formik.handleBlur}
                />
                {formik.touched.email && formik.errors.email ? (
                  <div>
                    <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                      {formik.errors.email}
                    </Form.Label>
                  </div>
                ) : null}
              </Form.Group>

              <Form.Group as={Col} controlId='mdp'>
                <Form.Label>Votre mot passe :</Form.Label>
                <Form.Control
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  type='password'
                  name='password'
                  placeholder='mot passe'
                  onBlur={formik.handleBlur}
                />
                {formik.touched.password && formik.errors.password ? (
                  <div>
                    <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                      {formik.errors.password}
                    </Form.Label>
                  </div>
                ) : null}
              </Form.Group>
            </Row>

            <Form.Group className='mb-3' controlId='adr'>
              <Form.Label>Address :</Form.Label>
              <Form.Control
                value={formik.values.address}
                onChange={formik.handleChange}
                placeholder='1234 xxxx '
                onBlur={formik.handleBlur}
                type='address'
                name='address'
              />
              {formik.touched.address && formik.errors.address ? (
                <div>
                  <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.address}
                  </Form.Label>
                </div>
              ) : null}
            </Form.Group>

            <Form.Group className='mb-3' controlId='code_postal'>
              <Form.Label>Code postale :</Form.Label>
              <Form.Control
                value={formik.values.code_postal}
                onChange={formik.handleChange}
                placeholder='code postale'
                onBlur={formik.handleBlur}
              />
              {formik.touched.code_postal && formik.errors.code_postal ? (
                <div>
                  <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.code_postal}
                  </Form.Label>
                </div>
              ) : null}
            </Form.Group>

            <Row className='mb-3'>
              <Form.Group className='mb-3' id='H'>
                <Form.Check
                  name='genre'
                  type='checkbox'
                  label='Homme'
                  value='Homme'
                  onBlur={formik.handleBlur}
                  checked={formik.values.genre === 'Homme'}
                  onChange={() => formik.setFieldValue('genre', 'Homme')}
                />
              </Form.Group>
              <Form.Group className='mb-3' id='F'>
                <Form.Check
                  type='checkbox'
                  label='Femme'
                  value='Femme'
                  onBlur={formik.handleBlur}
                  name='genre'
                  checked={formik.values.genre.includes('Femme')}
                  onChange={() => formik.setFieldValue('genre', 'Femme')}
                />
              </Form.Group>
            </Row>

            <Button
              type='submit'
              style={{
                backgroundColor: '#FEBF17',
                borderRadius: '10px',
                fontStyle: 'bold',
                fontSize: '20px',
              }}
              disabled={!(formik.isValid && formik.dirty)}
              variant='contained'>
              {' '}
              Modifier
            </Button>
          </Form>
        </div>
      )}
    </div>
  );
};

export default ProfileMain;
