/** @format */

import React, { useEffect } from 'react';
import './Profile.css';
import ProfileCard from '../Components/ProfileCard/ProfileCard';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import ProfileMain from './ProfileMain';
const Profile = () => {
  const [update, setupdate] = React.useState(false);
  const handle_updateshow = () => {
    console.log('here');
    setupdate(!update);
  };

  useEffect(() => {
    if (!window.location.hash) {
      window.location = window.location + '#loaded';
      window.location.reload();
    }
  }, []);
  useEffect(() => {
    console.log(update, 'update');
  }, [update]);

  function refreshPage() {
    window.location.reload(false);
  }
  return (
    <div style={{ marginTop: '30px', width: '100%' }}>
      <Grid container>
        <Grid
          item
          xs={12}
          sm={12}
          md={3}
          style={{ displayl: 'flex', justifyContent: 'center' }}>
          <ProfileCard showupdate={update} handleupdate={handle_updateshow} />
        </Grid>
        <Grid item xs={12} sm={12} md={9}>
          <Paper style={{ width: '100%', height: '100%' }}>
            <ProfileMain showupdate={update} handleupdate={handle_updateshow} />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default Profile;
