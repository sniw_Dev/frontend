/** @format */

import React, { useState, useEffect } from 'react';
import './lettres.css';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import CreateLetter from '../services/créerLettre';
import MediaCard from './MediaCard';
import { Get_letter } from '../api/lettres/LettresServices';
import { UserContext } from '../hooks/UserContext';
import { useContext } from 'react';

const Lettres = (props) => {
  const { user } = useContext(UserContext);

  const [lettres, setLettres] = useState();

  useEffect(() => {
    Get_letter(user._id).then((res) => {
      setLettres(res.data);
    });
  }, []);

  const refreshList = () => {
    Get_letter(user._id).then((res) => {
      setLettres(res.data);
      console.log(res.data);
    });
  };

  const searchbar = () => {
    return (
      <Grid item xs={12} sm={12} md={12}>
        <Paper
          elevation={3}
          style={{
            height: '10vh',
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <CreateLetter user={user._id} refreshList={refreshList} />
        </Paper>
      </Grid>
    );
  };

  const TypesLettres = () => {
    return (
      <Grid container spacing={3} xs={12} sm={12} md={12}>
        <Grid item xs={12} sm={4} md={4}>
          <Paper elevation={3} style={{ height: '40vh', padding: '20px' }}>
            <h4>Lettre ordinaire</h4>

            <span>Avantages</span>
            <ul>
              <li>Simple</li>
              <li>
                <p>
                  Délais de livraison raisonnables, en général dans les 72
                  heures qui suivent le jour de dépôt.
                </p>
              </li>
              <li>Prix économiques.</li>
              <li>Poids maximum: 2 Kg</li>
              <li>Lieu de dépôt: Boites postales ou En ligne.</li>
            </ul>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={4} md={4}>
          <Paper elevation={3} style={{ height: '40vh', padding: '20px' }}>
            <h4>Lettre recommandé</h4>
            <p>
              La lettre recommandée confère à votre courrier une valeur
              juridique et une possibilité de suivi de traçabilité.
            </p>
            <span>Avantages</span>
            <ul>
              <li>
                Preuve de réception signée par le destinataire ou son mandataire
              </li>
              <li>
                Indemnisation en cas de perte, détérioration ou spoliation.
              </li>
              <li>Poids maximum: 2 Kg</li>
            </ul>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={4} md={4}>
          <Paper elevation={3} style={{ height: '40vh', padding: '20px' }}>
            <h4>Lettre prioritaire</h4>
            <p>
              Pour vos envois ordinaires jusqu'à 20 grammes, la Poste Tunisienne
              vous propose le mode de traitement prioritaire.
            </p>
            <span>Avantages</span>
            <ul>
              <li>Simple</li>
              <li>
                Livraison rapide: Votre lettre sera distribuée dans les 48
                heures qui suivent le jour de dépôt
              </li>
            </ul>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  return (
    <div className='lettres_container'>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={12}>
          {TypesLettres()}
        </Grid>

        <Grid item xs={12} sm={12} md={12}>
          <div className='lettres_searchbar'>{searchbar()}</div>;
        </Grid>
        {lettres &&
          lettres.map((item) => (
            <Grid item xs={12} sm={12} md={3} style={{ height: '100%' }}>
              <MediaCard
                lettre={item}
                handleupdate={refreshList}
                user={user._id}
              />
            </Grid>
          ))}
      </Grid>
    </div>
  );
};
export default Lettres;
