/** @format */

import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import ScrollDialog from './ScrollDialog';
import { useEffect, useState } from 'react';
import { Delete_letter } from '../api/lettres/LettresServices';
import FormDialog from './FormDialog';
export default function MediaCard(props) {
  const MySwal = withReactContent(Swal);

  useEffect(() => {}, [props.lettre]);

  const handle_delete = () => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        Delete_letter(props.lettre._id).then((res) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');

          props.handleupdate();
        });
      }
    });
  };

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia
        component='img'
        height='140'
        image='/lettre.png'
        alt='green iguana'
      />
      <CardContent>
        <Typography gutterBottom variant='h5' component='div'>
          {props.lettre.type}
          <Typography variant='body2' color='text.secondary'>
            code lettre :{props.lettre.code_lettre}
          </Typography>
          <Typography variant='body2' color='text.secondary'>
            adresse destinataire : {props.lettre.adresse_destinataire}
          </Typography>

          <Typography variant='body2' color='text.secondary'>
            code postal :{props.lettre.code_postal}
          </Typography>
        </Typography>
      </CardContent>
      <CardActions>
        <ScrollDialog data={props.lettre.letter_body} />
        <FormDialog
          data={props.lettre}
          user={props.user}
          update={props.handleupdate}
        />
        <Button
          size='small'
          onClick={() => {
            handle_delete();
          }}>
          Supprimer
        </Button>
      </CardActions>
    </Card>
  );
}
