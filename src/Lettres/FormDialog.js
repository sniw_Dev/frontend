/** @format */

import React, { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { Edit_letter } from '../api/lettres/LettresServices';
import { useFormik } from 'formik';
import { Form } from 'react-bootstrap';

export default function FormDialog(props) {
  const [open, setOpen] = React.useState(false);

  const initialValues = {
    code_lettre: '',
    destinataire: '',
    letter_content: '',
    TypesLettres: '',
    code_postal: '',
  };

  const onSubmit = async (values) => {
    Edit_letter(props.data._id, generate_letter_Object())
      .then((res) => {
        console.log(res);
        props.update();
        handleClose();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const validate = (values) => {
    const errors = {};
    if (!values.code_lettre) {
      errors.code_lettre = 'oblgatoire';
    }
    if (!values.destinataire) {
      errors.destinataire = 'oblgatoire';
    }
    if (!values.letter_content) {
      errors.letter_content = 'oblgatoire';
    }
    if (!values.TypesLettres) {
      errors.TypesLettres = 'oblgatoire';
    }
    if (!values.code_postal) {
      errors.code_postal = 'oblgatoire';
    }
    return errors;
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  useEffect(() => {
    if (props.data) {
      formik.setFieldValue('code_lettre', props.data.code_lettre);
      formik.setFieldValue('destinataire', props.data.adresse_destinataire);
      formik.setFieldValue('letter_content', props.data.letter_body);
      formik.setFieldValue('TypesLettres', props.data.type);
      formik.setFieldValue('code_postal', props.data.code_postal);
    }
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const generate_letter_Object = () => {
    return {
      code_lettre: formik.values.code_lettre,
      adresse_destinataire: formik.values.destinataire,
      code_postal: formik.values.code_postal,
      type: formik.values.TypesLettres,
      letter_body: formik.values.letter_content,
      user_id: props.user,
    };
  };

  return (
    <div>
      <Button onClick={handleClickOpen}>Modifier</Button>
      <Dialog open={open} onClose={handleClose}>
        <Form onSubmit={formik.handleSubmit}>
          <DialogTitle>Modifier</DialogTitle>
          <DialogContent>
            {formik.touched.code_lettre && formik.errors.code_lettre ? (
              <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                {formik.errors.code_lettre}
              </FormLabel>
            ) : null}
            <TextField
              autoFocus
              margin='dense'
              label='code du  lettre'
              fullWidth
              variant='outlined'
              onChange={formik.handleChange}
              value={formik.values.code_lettre}
              type='text'
              id='code_lettre'
              onBlur={formik.handleBlur}
            />
            {formik.touched.destinataire && formik.errors.destinataire ? (
              <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                {formik.errors.destinataire}
              </FormLabel>
            ) : null}
            <TextField
              autoFocus
              margin='dense'
              label='Adresse du déstinataire'
              fullWidth
              variant='outlined'
              onChange={formik.handleChange}
              value={formik.values.destinataire}
              type='text'
              id='destinataire'
              onBlur={formik.handleBlur}
            />
            {formik.touched.code_postal && formik.errors.code_postal ? (
              <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                {formik.errors.code_postal}
              </FormLabel>
            ) : null}
            <TextField
              autoFocus
              margin='dense'
              label='Code postale'
              fullWidth
              variant='outlined'
              onChange={formik.handleChange}
              value={formik.values.code_postal}
              type='text'
              id='code_postal'
              onBlur={formik.handleBlur}
            />
            {formik.touched.letter_content && formik.errors.letter_content ? (
              <div>
                <span style={{ fontSize: '11px', color: 'red' }}>
                  {formik.errors.letter_content}
                </span>
              </div>
            ) : null}
            <FormLabel id='demo-row-radio-buttons-group-label'>
              Description
            </FormLabel>

            <TextField
              autoFocus
              margin='dense'
              placeholder=''
              multiline
              rows={6}
              maxRows={12}
              fullWidth
              variant='outlined'
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.letter_content}
              id='letter_content'
              name='letter_content'
            />

            <FormControl>
              <FormLabel id='demo-row-radio-buttons-group-label'>
                Type
              </FormLabel>
              <RadioGroup
                row
                aria-labelledby='demo-row-radio-buttons-group-label'
                name='row-radio-buttons-group'>
                <FormControlLabel
                  value='ordinaire'
                  control={<Radio />}
                  label='ordinaire'
                  name='TypesLettres'
                  onBlur={formik.handleBlur}
                  checked={formik.values.TypesLettres.includes('ordinaire')}
                  onChange={() =>
                    formik.setFieldValue('TypesLettres', 'ordinaire')
                  }
                />
                <FormControlLabel
                  value='prioritaire'
                  control={<Radio />}
                  label='prioritaire'
                  name='TypesLettres'
                  onBlur={formik.handleBlur}
                  checked={formik.values.TypesLettres.includes('prioritaire')}
                  onChange={() =>
                    formik.setFieldValue('TypesLettres', 'prioritaire')
                  }
                />
                <FormControlLabel
                  value='recommandé'
                  control={<Radio />}
                  label='recommandé'
                  name='TypesLettres'
                  onBlur={formik.handleBlur}
                  checked={formik.values.TypesLettres.includes('recommandé')}
                  onChange={() =>
                    formik.setFieldValue('TypesLettres', 'recommandé')
                  }
                />
              </RadioGroup>
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button type='submit' disabled={!(formik.isValid && formik.dirty)}>
              Modifier
            </Button>
          </DialogActions>
        </Form>
      </Dialog>
    </div>
  );
}
