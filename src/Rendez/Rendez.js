/** @format */

import React, { useEffect, useState } from 'react';
import './Rendez.css';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Button from '@mui/material/Button';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { ClipLoader } from 'react-spinners';
import { UserContext } from '../hooks/UserContext';
import { useContext } from 'react';
import {
  Create_Rdv,
  Get_Rdv,
  Delete_Rdv,
  Edit_Rdv,
} from '../api/redez_vous/RdvService';
import moment from 'moment';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { useFormik } from 'formik';

const Rendez = () => {
  const MySwal = withReactContent(Swal);

  const { user } = useContext(UserContext);
  const [RdvList, setRdvList] = useState([]);
  const [ActiveRdv, setActiveRdv] = useState();
  const [isloading, setIsloading] = React.useState(false);
  const [Modifier, setModifier] = React.useState(false);
  const [Email, setEmail] = React.useState('');
  const [Daterdv, setDaterdv] = React.useState(null);
  const [Heurerdv, setHeurerdv] = React.useState('Après midi');
  const [Confirmation, setConfirmation] = React.useState('Email');
  const [phone, setPhone] = React.useState('');
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const ITEM_HEIGHT = 48;

  const initialValues = {
    email: '',
    date_rendez_vous: '',
    temps_rendez_vous: '',
    confirmation: '',
    numero_telephone: '',
    user_id: user._id,
  };

  const onSubmit = async (values) => {
    console.log(values);
  };

  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = 'Email requis';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = 'Email invalide';
    }
    if (!values.date_rendez_vous) {
      errors.date_rendez_vous = 'Date requis';
    } else if (moment(values.date_rendez_vous).isBefore(moment())) {
      errors.date_rendez_vous = 'Date invalide';
    }
    if (!values.temps_rendez_vous) {
      errors.temps_rendez_vous = 'Temps requis';
    } else if (
      values.temps_rendez_vous !== 'Après midi' &&
      values.temps_rendez_vous !== 'Matin'
    ) {
      errors.temps_rendez_vous = 'Temps invalide';
    }
    if (!values.confirmation) {
      errors.confirmation = 'Confirmation requis';
    } else if (
      values.confirmation !== 'Email' &&
      values.confirmation !== 'tel'
    ) {
      errors.confirmation = 'Confirmation invalide';
    }
    if (!values.numero_telephone) {
      errors.numero_telephone = 'Numéro de téléphone requis';
    } else if (!/^[0-9]{8}$/.test(values.numero_telephone)) {
      errors.numero_telephone = 'Numéro de téléphone invalide';
    }
    return errors;
  };

  const formik = useFormik({ initialValues, validate, onSubmit });
  console.log(formik.errors);
  console.log(formik.touched);
  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    refreshList();
    formik.setTouched({}, false);
  }, []);

  useEffect(() => {
    console.log(ActiveRdv);

    if (Modifier === false) {
      setEmail('');
      setDaterdv(null);
      setHeurerdv('Après midi');
      setConfirmation('Email');
      setPhone('');
    } else {
      console.log(ActiveRdv);
    }
  }, [Modifier, ActiveRdv]);

  useEffect(() => {
    console.log(Daterdv);
  }, [Email, Daterdv, Heurerdv, Confirmation, phone, RdvList, ActiveRdv]);

  const handleAjout = () => {
    setIsloading(true);
    Create_Rdv(formik.values)
      .then((res) => {
        console.log(res);
        refreshList();

        setIsloading(false);
      })
      .catch((err) => {
        console.log(err);
        setIsloading(false);
      });
  };

  const refreshList = () => {
    Get_Rdv(user._id)
      .then((res) => {
        setRdvList(res.data.rendez_vous);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const HandleModifier = () => {
    setIsloading(true);
    Edit_Rdv(ActiveRdv._id, formik.values).then((res) => {
      console.log(res.data);
      refreshList();
      setIsloading(false);
    });
  };

  const HandleDelete = () => {
    handleClose();
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        Delete_Rdv(ActiveRdv._id).then((res) => {
          console.log(res);
          refreshList();
        });
      }
    });
  };

  const SwitchToModifier = () => {
    console.log(Daterdv);

    let rdv = ActiveRdv;
    formik.setValues({
      email: rdv.email,
      date_rendez_vous: rdv.date_rendez_vous,
      temps_rendez_vous: rdv.temps_rendez_vous,
      confirmation: rdv.confirmation,
      numero_telephone: rdv.numero_telephone,
    });

    // setEmail(rdv.email);
    // setDaterdv(ActiveRdv.date_rendez_vous);
    // setHeurerdv(rdv.temps_rendez_vous);
    // setConfirmation(rdv.confirmation);
    // setPhone(rdv.numero_telephone);

    setModifier(true);
    handleClose();
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const LongMenu = (rdv) => {
    return (
      <div>
        <IconButton
          aria-label='more'
          id='long-button'
          aria-controls={open ? 'long-menu' : undefined}
          aria-expanded={open ? 'true' : undefined}
          aria-haspopup='true'
          onClick={handleClick}>
          <MoreVertIcon />
        </IconButton>
        <Menu
          id='long-menu'
          MenuListProps={{
            'aria-labelledby': 'long-button',
          }}
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: '20ch',
              boxShadow: '0.2px 0.2px 1px 0.5px',
            },
          }}>
          <MenuItem
            onClick={(e) => {
              SwitchToModifier(rdv);
            }}>
            Modifier
          </MenuItem>
          <MenuItem onClick={HandleDelete}>Supprimer</MenuItem>
        </Menu>
      </div>
    );
  };
  const BasicTable = () => {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell align='right'>id</TableCell>
              <TableCell align='right'>email</TableCell>
              <TableCell align='right'>numero_telephone</TableCell>
              <TableCell align='right'>date rendezvous</TableCell>
              <TableCell align='right'>temps rendezvous</TableCell>
              <TableCell align='right'>confirmation</TableCell>
              <TableCell align='right'>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {RdvList.map((row, index) => (
              <TableRow
                key={index}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell align='right'>{index}</TableCell>

                <TableCell align='right'>{row.email}</TableCell>
                <TableCell align='right'>{row.numero_telephone}</TableCell>
                <TableCell align='right'>
                  {moment(row.date_rendez_vous).format('DD-MM-YYYY')}
                </TableCell>
                <TableCell align='right'>{row.temps_rendez_vous}</TableCell>
                <TableCell align='right'>{row.confirmation}</TableCell>
                <TableCell
                  align='right'
                  onClick={(e) => {
                    setActiveRdv(row);
                  }}>
                  {LongMenu(row)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  const formLeftAjout = () => {
    return (
      <Paper
        elevation={3}
        style={{
          height: 'fit-content',
          padding: '10px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          position: 'sticky',
        }}>
        {isloading ? (
          <div
            style={{
              marginTop: '30px',
              width: '100%',
              height: 'fit-content',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ClipLoader size={150} color={'#123abc'} loading={isloading} />
          </div>
        ) : (
          <>
            <div className='iconholder'>
              <CalendarMonthIcon
                style={{ fontSize: '100px', color: '#333333' }}
              />
            </div>
            {formik.touched.email && formik.errors.email ? (
              <div>
                <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                  {formik.errors.email}
                </FormLabel>
              </div>
            ) : null}
            <TextField
              autoFocus
              margin='dense'
              id='email'
              label=' adresse email:'
              fullWidth
              variant='outlined'
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />

            {formik.touched.numero_telephone &&
            formik.errors.numero_telephone ? (
              <div>
                <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                  {formik.errors.numero_telephone}
                </FormLabel>
              </div>
            ) : null}
            <TextField
              autoFocus
              margin='dense'
              id='numero_telephone'
              name='numero_telephone'
              label='Numéro du télèphone'
              fullWidth
              variant='outlined'
              value={formik.values.numero_telephone}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />

            <LocalizationProvider
              onBlur={formik.handleBlur}
              dateAdapter={AdapterDateFns}>
              {formik.errors.date_rendez_vous ? (
                <div>
                  <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.date_rendez_vous}
                  </FormLabel>
                </div>
              ) : null}
              <DatePicker
                label='Date Rendez-vous'
                name='date_rendez_vous'
                value={formik.values.date_rendez_vous}
                onChange={(event) =>
                  formik.setFieldValue('date_rendez_vous', event)
                }
                renderInput={(params) => (
                  <TextField fullWidth margin='dense' {...params} />
                )}
              />
            </LocalizationProvider>

            <FormControl>
              {formik.touched.temps_rendez_vous &&
              formik.errors.temps_rendez_vous ? (
                <div>
                  <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.temps_rendez_vous}
                  </FormLabel>
                </div>
              ) : null}
              <FormLabel id='demo-radio-buttons-group-label'>
                chosir une date
              </FormLabel>

              <RadioGroup
                row
                aria-labelledby='demo-row-radio-buttons-group-label'>
                <FormControlLabel
                  name='temps_rendez_vous'
                  onChange={(event) => {
                    formik.setFieldValue(
                      'temps_rendez_vous',
                      event.target.value
                    );
                  }}
                  onBlur={formik.handleBlur}
                  value='Après midi'
                  control={<Radio />}
                  label='Après midi'
                />
                <FormControlLabel
                  value='Matin'
                  control={<Radio />}
                  label='Matin'
                  name='temps_rendez_vous'
                  onChange={(event) => {
                    formik.setFieldValue(
                      'temps_rendez_vous',
                      event.target.value
                    );
                  }}
                  onBlur={formik.handleBlur}
                />
              </RadioGroup>
            </FormControl>
            <FormControl>
              {formik.touched.confirmation && formik.errors.confirmation ? (
                <div>
                  <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.confirmation}
                  </FormLabel>
                </div>
              ) : null}
              <FormLabel id='demo-radio-buttons-group-label'>
                Confirmation demandée par :
              </FormLabel>

              <RadioGroup
                row
                aria-labelledby='demo-row-radio-buttons-group-label'
                name='row-radio-buttons-group'>
                <FormControlLabel
                  name='confirmation'
                  onChange={(event) => {
                    formik.setFieldValue('confirmation', event.target.value);
                  }}
                  onBlur={formik.handleBlur}
                  value='tel'
                  control={<Radio />}
                  label='Appel téléphonique'
                />
                <FormControlLabel
                  value='Email'
                  control={<Radio />}
                  label='Email'
                  name='confirmation'
                  onChange={(event) => {
                    formik.setFieldValue('confirmation', event.target.value);
                  }}
                  onBlur={formik.handleBlur}
                />
              </RadioGroup>
            </FormControl>
            <Button
              fullWidth
              variant='outlined'
              disabled={!(formik.isValid && formik.dirty)}
              onClick={handleAjout}>
              Ajouter
            </Button>
          </>
        )}
      </Paper>
    );
  };

  const formLeftModifier = () => {
    return (
      <Paper
        elevation={3}
        style={{
          height: 'fit-content',
          padding: '10px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          position: 'sticky',
        }}>
        {isloading ? (
          <div
            style={{
              marginTop: '30px',
              width: '100%',
              height: 'fit-content',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ClipLoader size={150} color={'#123abc'} loading={isloading} />
          </div>
        ) : (
          <>
            <div className='iconholder'>
              <CalendarMonthIcon
                style={{ fontSize: '100px', color: '#333333' }}
              />
            </div>
            {formik.touched.email && formik.errors.email ? (
              <div>
                <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                  {formik.errors.email}
                </FormLabel>
              </div>
            ) : null}
            <TextField
              autoFocus
              margin='dense'
              id='email'
              label=' adresse email:'
              fullWidth
              variant='outlined'
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />

            {formik.touched.numero_telephone &&
            formik.errors.numero_telephone ? (
              <div>
                <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                  {formik.errors.numero_telephone}
                </FormLabel>
              </div>
            ) : null}
            <TextField
              autoFocus
              margin='dense'
              id='numero_telephone'
              name='numero_telephone'
              label='Numéro du télèphone'
              fullWidth
              variant='outlined'
              value={formik.values.numero_telephone}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />

            <LocalizationProvider
              onBlur={formik.handleBlur}
              dateAdapter={AdapterDateFns}>
              {formik.errors.date_rendez_vous ? (
                <div>
                  <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.date_rendez_vous}
                  </FormLabel>
                </div>
              ) : null}
              <DatePicker
                label='Date Rendez-vous'
                name='date_rendez_vous'
                value={formik.values.date_rendez_vous}
                onChange={(event) =>
                  formik.setFieldValue('date_rendez_vous', event.toISOString())
                }
                renderInput={(params) => (
                  <TextField fullWidth margin='dense' {...params} />
                )}
              />
            </LocalizationProvider>

            <FormControl>
              {formik.touched.temps_rendez_vous &&
              formik.errors.temps_rendez_vous ? (
                <div>
                  <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.temps_rendez_vous}
                  </FormLabel>
                </div>
              ) : null}
              <FormLabel id='demo-radio-buttons-group-label'>
                chosir une date
              </FormLabel>

              <RadioGroup
                row
                aria-labelledby='demo-row-radio-buttons-group-label'>
                <FormControlLabel
                  name='temps_rendez_vous'
                  onChange={(event) => {
                    formik.setFieldValue(
                      'temps_rendez_vous',
                      event.target.value
                    );
                  }}
                  onBlur={formik.handleBlur}
                  value='Après midi'
                  checked={formik.values.temps_rendez_vous === 'Après midi'}
                  control={<Radio />}
                  label='Après midi'
                />
                <FormControlLabel
                  value='Matin'
                  control={<Radio />}
                  checked={formik.values.temps_rendez_vous === 'Matin'}
                  label='Matin'
                  name='temps_rendez_vous'
                  onChange={(event) => {
                    formik.setFieldValue(
                      'temps_rendez_vous',
                      event.target.value
                    );
                  }}
                  onBlur={formik.handleBlur}
                />
              </RadioGroup>
            </FormControl>
            <FormControl>
              {formik.touched.confirmation && formik.errors.confirmation ? (
                <div>
                  <FormLabel style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.confirmation}
                  </FormLabel>
                </div>
              ) : null}
              <FormLabel id='demo-radio-buttons-group-label'>
                Confirmation demandée par :
              </FormLabel>

              <RadioGroup
                row
                aria-labelledby='demo-row-radio-buttons-group-label'
                name='row-radio-buttons-group'>
                <FormControlLabel
                  name='confirmation'
                  onChange={(event) => {
                    formik.setFieldValue('confirmation', event.target.value);
                  }}
                  onBlur={formik.handleBlur}
                  value='tel'
                  control={<Radio />}
                  label='Appel téléphonique'
                  checked={formik.values.confirmation === 'tel'}
                />
                <FormControlLabel
                  value='Email'
                  control={<Radio />}
                  label='Email'
                  name='confirmation'
                  checked={formik.values.confirmation === 'Email'}
                  onChange={(event) => {
                    formik.setFieldValue('confirmation', event.target.value);
                  }}
                  onBlur={formik.handleBlur}
                />
              </RadioGroup>
            </FormControl>
            <Button variant='outlined' onClick={() => setModifier(false)}>
              Retour
            </Button>
            <Button
              variant='outlined'
              disabled={!(formik.isValid && formik.dirty)}
              onClick={HandleModifier}>
              Modifier
            </Button>
          </>
        )}
      </Paper>
    );
  };

  return (
    <Grid container spacing={3} xs={12} sm={12} md={12}>
      <Grid item xs={12} sm={3} md={3} style={{ position: 'sticky' }}>
        {Modifier ? formLeftModifier() : formLeftAjout()}
      </Grid>
      <Grid item xs={12} sm={9} md={9}>
        {RdvList && BasicTable()}
      </Grid>
    </Grid>
  );
};

export default Rendez;
