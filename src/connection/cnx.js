/** @format */

import React, { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import PersonIcon from '@mui/icons-material/Person';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Fade from '@mui/material/Fade';
import useAuth from './../hooks/useAuth';
import { useFormik } from 'formik';
import './cnx.css';
function Cnx(props) {
  const initialValues = {
    email: '',
    password: '',
  };
  const onSubmit = async (values) => {
    await loginUser({
      email: formik.values.email,
      password: formik.values.password,
    });
  };

  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = 'Invalid email address';
    }
    if (!values.password) {
      errors.password = 'Required';
    }
    return errors;
  };
  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { loginUser, error } = useAuth();

  const handleSubmit = async (e) => {
    e.preventDefault();
    await loginUser({ email, password });
  };

  const handle_email_change = (e) => {
    setEmail(e.target.value);
    console.log(e.target.value);
  };

  const handle_password_change = (e) => {
    setPassword(e.target.value);
    console.log(e.target.value);
  };
  console.log(formik.errors);
  return (
    <Fade in={true}>
      <div className='connexion'>
        <div style={{ width: '100%', display: 'flex' }}>
          <Button
            style={{
              borderRadius: '10px',
              fontStyle: 'bold',
              fontSize: '20px',
              color: 'white',
              marginBottom: '20px',
            }}
            onClick={() => props.retour()}
            variant='text'
            startIcon={<ArrowBackIcon />}>
            Retour
          </Button>
        </div>
        <div className='Fcnx'>
          <Form onSubmit={formik.handleSubmit}>
            <Form.Group className='mb-1' controlId='email'>
              <Form.Label> adresse Email</Form.Label>
              {formik.errors.email ? (
                <div>
                  <Form.Label style={{ fontSize: '18px', color: 'red' }}>
                    {formik.errors.email}
                  </Form.Label>
                </div>
              ) : null}
              <Form.Control
                value={formik.values.email}
                onChange={formik.handleChange}
                type='email'
                placeholder='xxx@Exemple.com'
              />
            </Form.Group>

            <Form.Group className='mb-3' controlId='password'>
              <Form.Label>Password</Form.Label>
              {formik.errors.password ? (
                <div>
                  <Form.Label style={{ fontSize: '18px', color: 'red' }}>
                    {formik.errors.password}
                  </Form.Label>
                </div>
              ) : null}
              <Form.Control
                value={formik.values.password}
                onChange={formik.handleChange}
                type='password'
                placeholder='Password'
              />
            </Form.Group>
            <Form.Group className='mb-3' controlId='validation'>
              <Form.Check type='checkbox' label='Vérifiez-moi' />
            </Form.Group>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button
                style={{
                  backgroundColor: '#FEBF17',
                  borderRadius: '10px',
                  fontStyle: 'bold',
                  fontSize: '20px',
                }}
                type='submit'
                variant='contained'
                startIcon={<PersonIcon />}>
                Se connecter
              </Button>
            </div>
          </Form>
        </div>
      </div>
    </Fade>
  );
}
//}
export default Cnx;
