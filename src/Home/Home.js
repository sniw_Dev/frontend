/** @format */

import React, { useState, useEffect } from 'react';
import Banner from '../Components/Banner/Banner';
import Container from '@mui/material/Container';
import ServericeCard from '../Components/ServiceCard/ServericeCard';
import Grid from '@mui/material/Grid';
import Profile from '../Profile/Profile';
import Lettres from '../Lettres/Lettres';
import Rendez from '../Rendez/Rendez';
import Colis from '../Colis/Colis';
import Button from '@mui/material/Button';

const Home = () => {
  const [ActivePage, setActivePage] = useState('profile');

  useEffect(() => {}, [ActivePage]);
  const handlelogout = () => {
    localStorage.removeItem('token');
    window.location.reload();
  };
  return (
    <Container
      maxWidth='xl'
      style={{
        backgroundColor: '#091244',
        height: '100%',
        minHeight: '1000px',
      }}>
      <div className='Home_container'>
        <Grid
          container
          spacing={3}
          style={{ zIndex: 10, position: 'relative', top: '20px' }}>
          <Grid
            item
            xs={12}
            sm={12}
            md={12}
            style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button
              variant='outlined'
              style={{
                backgroundColor: '#fab13c',

                color: '#fff',
              }}
              onClick={handlelogout}>
              logout
            </Button>
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            md={3}
            onClick={() => {
              setActivePage('profile');
            }}>
            <ServericeCard ActivePage={ActivePage} titre={'profile'} />
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            md={3}
            onClick={() => {
              setActivePage('Lettres');
            }}>
            <ServericeCard ActivePage={ActivePage} titre={'Lettres'} />
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            md={3}
            onClick={() => {
              setActivePage('Colis');
            }}>
            <ServericeCard ActivePage={ActivePage} titre={'Colis'} />
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            md={3}
            onClick={() => {
              setActivePage('rendez-vous');
            }}>
            <ServericeCard ActivePage={ActivePage} titre={'rendez-vous'} />
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} md={12}>
            {ActivePage === 'rendez-vous' && <Rendez />}
            {ActivePage === 'Colis' && <Colis />}
            {ActivePage === 'Lettres' && <Lettres />}
            {ActivePage === 'profile' && <Profile />}
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Home;
