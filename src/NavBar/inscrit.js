/** @format */

import React, { useState } from 'react';
import { Form, Row, Col, Modal } from 'react-bootstrap';
import Button from '@mui/material/Button';
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import ClipLoader from 'react-spinners/ClipLoader';
import useAuth from './../hooks/useAuth';
import { useFormik } from 'formik';

function Inscrit() {
  const initialValues = {
    email: '',
    password: '',
    nom: '',
    prenom: '',
    address: '',
    code_postal: '',
    image: '',
    genre: 'H',
  };

  const onSubmit = async (values) => {
    console.log(values);
    setloading(true);
    const formData = new FormData();
    formData.append('nom', values.nom);
    formData.append('email', values.email);
    formData.append('password', values.password);
    formData.append('prenom', values.prenom);
    formData.append('address', values.address);
    formData.append('codepostal', values.code_postal);
    formData.append('genre', values.genre);
    formData.append('image', values.image);
    console.log('formData:', formData);
    registerUser(formData)
      .then((res) => {
        setloading(false);
        formik.resetForm();
      })
      .catch((err) => {
        console.log(err.response);
        alert(err.response.data.message);
        setloading(false);
      });
  };

  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = 'obligatoire';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = 'Invalid email address';
    }
    if (!values.password) {
      errors.password = 'obligatoire';
    } else if (
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/i.test(
        values.password
      )
    ) {
      errors.password =
        'password doit contenir au moins 8 caractères dont au moins une majuscule, une minuscule, un chiffre et un caractère spécial';
    }

    if (!values.nom) {
      errors.nom = 'obligatoire';
    } else if (!/^[a-zA-Z]+$/i.test(values.nom)) {
      errors.nom = 'nom doit contenir que des lettres';
    }
    if (!values.prenom) {
      errors.prenom = 'obligatoire';
    } else if (!/^[a-zA-Z]+$/i.test(values.prenom)) {
      errors.prenom = 'prenom doit etre lettres seulement';
    }
    if (!values.address) {
      errors.address = 'obligatoire';
    }
    if (!values.code_postal) {
      errors.code_postal = 'Required';
    } else if (!/^[0-9]{5}$/i.test(values.code_postal)) {
      errors.code_postal = 'code postal doit etre un nombre de 5 chiffres';
    }
    if (!values.image) {
      errors.image = 'obligatoire';
    }
    return errors;
  };
  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  //fent
  const { registerUser, error } = useAuth();

  const [show, setShow] = useState(false);

  const [loading, setloading] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div className='inscrit'>
      <Modal show={show} onHide={handleClose} variant='primary'>
        <Modal.Header
          style={{ backgroundColor: 'rgb(254, 191, 23)' }}
          closeButton>
          <Modal.Title>
            <h5 style={{ color: '#fff' }}>Bienvenue sur la poste tunisienne</h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {loading ? (
            <div
              style={{
                width: '100%',
                height: '400px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ClipLoader color={'#050922'} loading={loading} size={150} />
            </div>
          ) : (
            <Form onSubmit={formik.handleSubmit}>
              <Row className='mb-3'>
                <Form.Group as={Col} controlId='nom'>
                  <Form.Label>Nom:</Form.Label>

                  <Form.Control
                    value={formik.values.nom}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched.nom && formik.errors.nom ? (
                    <div>
                      <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                        {formik.errors.nom}
                      </Form.Label>
                    </div>
                  ) : null}
                </Form.Group>
                <Form.Group as={Col} controlId='prenom'>
                  <Form.Label>Prénom :</Form.Label>

                  <Form.Control
                    value={formik.values.prenom}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched.prenom && formik.errors.prenom ? (
                    <div>
                      <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                        {formik.errors.prenom}
                      </Form.Label>
                    </div>
                  ) : null}
                </Form.Group>
              </Row>

              <Row className='mb-3'>
                <Form.Group as={Col} controlId='email'>
                  <Form.Label>Email :</Form.Label>

                  <Form.Control
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    type='Email'
                    placeholder='XXX@EXEMPLE.com'
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched.email && formik.errors.email ? (
                    <div>
                      <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                        {formik.errors.email}
                      </Form.Label>
                    </div>
                  ) : null}
                </Form.Group>

                <Form.Group as={Col} controlId='password'>
                  <Form.Label>Votre mot passe :</Form.Label>

                  <Form.Control
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    type='password'
                    name='password'
                    placeholder='mot passe'
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched.password && formik.errors.password ? (
                    <div>
                      <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                        {formik.errors.password}
                      </Form.Label>
                    </div>
                  ) : null}
                </Form.Group>
              </Row>

              <Form.Group className='mb-3' controlId='address'>
                <Form.Label>Address :</Form.Label>

                <Form.Control
                  value={formik.values.address}
                  onChange={formik.handleChange}
                  placeholder='1234 xxxx '
                  onBlur={formik.handleBlur}
                />
                {formik.touched.address && formik.errors.address ? (
                  <div>
                    <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                      {formik.errors.address}
                    </Form.Label>
                  </div>
                ) : null}
              </Form.Group>

              <Form.Group className='mb-3' controlId='code_postal'>
                <Form.Label>Code postale :</Form.Label>

                <Form.Control
                  value={formik.values.code_postal}
                  onChange={formik.handleChange}
                  placeholder='code postale'
                  onBlur={formik.handleBlur}
                />
                {formik.touched.code_postal && formik.errors.code_postal ? (
                  <div>
                    <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                      {formik.errors.code_postal}
                    </Form.Label>
                  </div>
                ) : null}
              </Form.Group>

              <Row className='mb-3'>
                <Form.Group className='mb-3' id='H'>
                  <Form.Check
                    name='genre'
                    type='checkbox'
                    label='Homme'
                    value='Homme'
                    onBlur={formik.handleBlur}
                    checked={formik.values.genre === 'Homme'}
                    onChange={() => formik.setFieldValue('genre', 'Homme')}
                  />
                </Form.Group>
                <Form.Group className='mb-3' id='F'>
                  <Form.Check
                    type='checkbox'
                    label='Femme'
                    value='Femme'
                    onBlur={formik.handleBlur}
                    name='genre'
                    checked={formik.values.genre.includes('Femme')}
                    onChange={() => formik.setFieldValue('genre', 'Femme')}
                  />
                </Form.Group>
              </Row>

              <Form.Group controlId='image' className='mb-3'>
                <Form.Label>Image du carte d'identité :</Form.Label>

                <Form.Control
                  id='image'
                  type='file'
                  onBlur={formik.handleBlur}
                  onChange={(e) => {
                    formik.setFieldValue('image', e.target.files[0]);
                  }}
                />
                {formik.touched.image && formik.errors.image ? (
                  <div>
                    <Form.Label style={{ fontSize: '11px', color: 'red' }}>
                      {formik.errors.image}
                    </Form.Label>
                  </div>
                ) : null}
              </Form.Group>
              <Button
                style={{
                  backgroundColor: '#FEBF17',
                  borderRadius: '10px',
                  fontStyle: 'bold',
                  fontSize: '20px',
                }}
                type='submit'
                variant='contained'
              git statu>
                {' '}
                S'inscrire
              </Button>
            </Form>
          )}
        </Modal.Body>
      </Modal>

      <Button
        style={{
          backgroundColor: '#FEBF17',
          borderRadius: '10px',
          fontStyle: 'bold',
          fontSize: '20px',
        }}
        variant='contained'
        startIcon={<PersonAddAltIcon />}
        onClick={handleShow}>
        Devenir abonné
      </Button>
    </div>
  );
}

export default Inscrit;
