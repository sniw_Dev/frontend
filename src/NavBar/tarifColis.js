import React,{useState} from 'react';
import {Table, Modal, Button} from 'react-bootstrap';
function TarifColis() {
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);
  return (
    <div className='tarifC'>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title><h5>Bienvenue sur l'Assistent de la Poste</h5></Modal.Title>
        </Modal.Header>
        <Modal.Body>

       <h5>Tarifs des Colis Postaux du régime intérieur</h5> 
       <Table striped bordered hover>
         <thead>
          <tr><th>#</th> <th>Prix</th>  </tr>
        </thead>
        <tbody>
        <tr><td>Taxe fixe par Colis de 2 Kg</td><td>4,300 DT</td></tr>
        <tr><td>Taxe par 1 kg ou fraction de 1 kg supplémentaire </td><td>0,300 DT</td></tr>
        </tbody> 
       </Table> 
       <h5> Tarifs des Colis du régime international</h5> 
       </Modal.Body>
       <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
         Close
        </Button> 
         </Modal.Footer>
       </Modal>
       <Button variant="light " onClick={handleShow}>Les tarifs des colis</Button> 
    </div>

    
  );
}

export default TarifColis;