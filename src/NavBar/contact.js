import React, {useState} from 'react';
import {Form, Button, Modal} from 'react-bootstrap';
function Contact() {
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);
  return (


    <div className='contacter'>
     <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title><h5>Bienvenue sur l'Assistent de la Poste</h5></Modal.Title>
        </Modal.Header>
        <Modal.Body>

      <br></br>
      <div className='msg'>
        <p>Bonjour, je suis l'assistant de la poste, disponible 7j/7 et 24h/24.</p>
        <p>Avant de commancer, je doit sauvgarder notre message. Merci d'éviter de me 
        communique vos données personnelles sensible.</p>
        <p> Je suis la pour vous aider</p>
        </div>
        <br></br>
      <div className='formulaire'> 
      <Form>
      <Form.Group className="mb-3" controlId="adrE">
      <Form.Label>Email address</Form.Label>
      <Form.Control type="email" placeholder="name@example.com" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
      <Form.Label>Votre message</Form.Label>
      <Form.Control as="textarea" rows={3} />
      </Form.Group>
      </Form> 
      </div>
      



      <br></br>
      <div className='outilC'> 
      <p>adresse email : brc@poste.tn</p>
      <p>adresse : Rue Hédi Nouira -1030 Tunis, Tunisie</p>
      <p>Téléphone : (+216) 71 839 000 - Fax : (+216) 71 839 113</p>
      <p>Adresse du parking le plus proche :Rue Pierre de Coubertin</p>
      <p>Horaire de Travail : </p>
      <ul> 
      <li> Du Lundi au Jeudi : de 8h00 à 13h00 et de 14h00 à 17h00.  </li>
      <li> Le Vendredi : de 8h00 à 12h00 et de 14h00 à 17h00.</li>
      </ul>
      </div>
      </Modal.Body>
       
      <Modal.Footer>
      <Button variant="secondary" onClick={handleClose}>
        Close
        </Button> 
        </Modal.Footer>
      </Modal>
      <Button variant="light"  onClick={handleShow}>Contacter nous</Button>
      
</div> );
}

export default Contact;