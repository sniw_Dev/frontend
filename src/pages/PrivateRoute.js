/** @format */

import React, { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

import { UserContext } from './../hooks/UserContext';
import ClipLoader from 'react-spinners/ClipLoader';
import { useNavigate } from 'react-router-dom';

export default function PrivateRoute(props) {
  const { user, isLoading } = useContext(UserContext);
  useEffect(() => {
    console.log(user);
  }, [user]);

  if (isLoading) {
    return (
      <div
        style={{
          width: '100vw',
          height: '100vh',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <ClipLoader color={'#050922'} loading={isLoading} size={150} />
      </div>
    );
  }
  if (localStorage.getItem('token') !== null) {
    console.log(props);

    return props.children;
  }
  //redirect if there is no user
  return <Navigate to='/' replace={true} />;
}
