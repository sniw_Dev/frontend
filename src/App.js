/** @format */
import { useState, useEffect } from 'react';

import './App.css';
import Home from './Home/Home';
import Header from './headerPA/header';
import Lettre from './services/E-lettre';
import Colis from './services/E-colis';
import RendezVous from './services/Rendezvous';
import Modéle from './services/achat';
import Payer from './services/payer';
import { Routes, Route } from 'react-router-dom';
import HeaderPageAcu from './headerPageACU/HeaderPageAcu';
import Consultation from './services/CRUDprofile';
import Modelelettres from './services/ModeleLettre';
import { UserContext } from './hooks/UserContext';
import useFindUser from './hooks/useFindUser';
import PrivateRoute from './pages/PrivateRoute';

function App() {
  const { user, setUser, isLoading } = useFindUser();

  return (
    <div className='app'>
      <UserContext.Provider value={{ user, setUser, isLoading }}>
        <Routes>
          <Route
            path='/service'
            element={
              <PrivateRoute>
                <Home />
              </PrivateRoute>
            }
          />
          <Route
            path='/E-lettre'
            element={
              <PrivateRoute>
                <Lettre />
              </PrivateRoute>
            }
          />
          <Route
            path='/E-colis'
            element={
              <PrivateRoute>
                <Colis />
              </PrivateRoute>
            }
          />
          <Route
            path='/rendez'
            element={
              <PrivateRoute>
                <RendezVous />
              </PrivateRoute>
            }
          />
          <Route
            path='/navLettre'
            element={
              <PrivateRoute>
                <Modéle />
              </PrivateRoute>
            }
          />
          <Route
            path='/payer'
            element={
              <PrivateRoute>
                <Payer />
              </PrivateRoute>
            }
          />
          <Route
            path='/consulter'
            element={
              <PrivateRoute>
                <Consultation />
              </PrivateRoute>
            }
          />
          <Route path='/' element={<HeaderPageAcu />} />
          <Route
            path='/ModeleLettre'
            element={
              <PrivateRoute>
                <Modelelettres />
              </PrivateRoute>
            }
          />
        </Routes>
      </UserContext.Provider>
    </div>
  );
}

export default App;
