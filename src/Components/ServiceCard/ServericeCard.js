/** @format */

import { useEffect } from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import SkipNextIcon from '@mui/icons-material/SkipNext';
import { makeStyles } from '@mui/styles';

const ServericeCard = (props) => {
  const theme = useTheme();
  useEffect(() => {}, [props.ActiveCard]);
  const styles = makeStyles({
    card: {
      display: 'flex',
      height: '60%',
      cursor: 'pointer',

      '&:hover': {
        'box-shadow': 'rgba(0, 0, 0, 0.22) 0px 19px 43px',
        transform: 'translate3d(0px, -5px, 0px)',
        scale: 2.5,
        backgroundColor: '#fab13c',
        color: '#fff',
      },
    },
    ActiveCard: {
      display: 'flex',
      height: '60%',
      cursor: 'pointer',
      backgroundColor: '#fab13c',
      color: '#fff',
    },
  });
  const classes = styles();

  const renderImage = () => {
    if (props.titre === 'profile') {
      return (
        <CardMedia component='img' sx={{ width: 100 }} image='/profile.png' />
      );
    } else if (props.titre === 'Lettres') {
      return (
        <CardMedia component='img' sx={{ width: 100 }} image='/letter.png' />
      );
    } else if (props.titre === 'Colis') {
      return (
        <CardMedia component='img' sx={{ width: 100 }} image='/colis.png' />
      );
    } else if (props.titre === 'rendez-vous') {
      return (
        <CardMedia
          component='img'
          sx={{ width: 151 }}
          image='/rendezvous.png'
        />
      );
    }
  };
  return (
    <Card
      className={
        props.ActivePage === props.titre ? classes.ActiveCard : classes.card
      }>
      {renderImage()}
      <Box sx={{ display: 'flex', flexDirection: 'column' }}>
        <CardContent
          sx={{
            flex: '1 0 auto',
            alignItems: 'center',
            display: 'flex',
          }}>
          <Typography component='div' variant='h5' style={{ display: 'flex' }}>
            {props.titre}
          </Typography>
        </CardContent>
      </Box>
    </Card>
  );
};

export default ServericeCard;
