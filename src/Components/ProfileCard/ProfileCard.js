/** @format */

import React, { useState, useEffect } from 'react';
import { useContext } from 'react';
import './ProfileCard.css';
import { UserContext } from '../../hooks/UserContext';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { Deleteuser } from '../../api/users/UserService';
import { useNavigate } from 'react-router-dom';
import { Getuser } from '../../api/users/UserService';
const MySwal = withReactContent(Swal);

const ProfileCard = (props) => {
  const [user, setuser] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    Getuser().then((res) => {
      setuser(res.data);
    });
  }, []);
  useEffect(() => {
    console.log(user, 'userProfileCard');
  }, [user]);

  const handle_delete = () => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        Deleteuser().then((res) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');

          localStorage.removeItem('token');
          navigate(0);
        });
      }
    });
  };
  return (
    <div>
      {user && (
        <div class='card-container'>
          <span class='pro'>PROFILE</span>
          <img
            class='round'
            style={{ width: '250px', height: '250px' }}
            src='/user.png'
            alt='user'
          />
          <h3>{`${user.nom} ${user.prenom}`}</h3>
          <h6>{user.email}</h6>

          <div class='buttons'>
            <button
              class='primary ghost'
              onClick={() => {
                props.handleupdate();
              }}>
              Modifier
            </button>
            <button
              class='primary ghost'
              onClick={() => {
                handle_delete();
              }}>
              Supprimer
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProfileCard;
