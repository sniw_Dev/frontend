/** @format */

import React from 'react';
import Paper from '@mui/material/Paper';
import './Banner.css';
const Banner = () => {
  return (
    <div>
      <Paper
        className='bannercontainer'
        style={{ backgroundColor: '#fff', borderRadius: '10px' }}
        variant='outlined'
        elevation={24}>
        <div className='text_section'>
          <span>Bienvenue , Test !</span>
          <p>
            La Poste Encore plus proche On met à la disposition de nos clientèle
            un ensemble de services en ligne.
          </p>
        </div>
        <div className='picture_section'>
          <img src='/welcome.png' alt='welcome' />
        </div>
      </Paper>
    </div>
  );
};

export default Banner;
