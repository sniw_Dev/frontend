/** @format */

import axios from 'axios';
import { APIURL, Token_Header } from '../../config/Config';
let user = JSON.parse(localStorage.getItem('user'));
export const updateUser = (user) => {
  return axios.put(`${APIURL}/user/update`, user, Token_Header());
};

export const Deleteuser = (id) => {
  return axios.delete(`${APIURL}/user/delete/`, Token_Header());
};

export const Getuser = () => {
  console.log(user, 'userService');
  return axios.get(`${APIURL}/user/`, Token_Header());
};
