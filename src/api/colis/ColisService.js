/** @format */
import axios from 'axios';
import { APIURL, Token_Header } from '../../config/Config';

export const Create_colis = (colis) => {
  return axios.post(`${APIURL}/colis/create`, colis, Token_Header());
};

export const Delete_colis = (id) => {
  return axios.delete(`${APIURL}/colis/${id}`, Token_Header());
};

export const Edit_colis = (id, colis) => {
  return axios.put(`${APIURL}/colis/${id}`, colis, Token_Header());
};

export const Get_colis = (id) => {
  return axios.get(`${APIURL}/colis/user/${id}`, Token_Header());
};
