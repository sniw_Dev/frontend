/** @format */

import axios from 'axios';
import { APIURL, Token_Header } from '../../config/Config';

export const Create_Rdv = (rdv) => {
  return axios.post(`${APIURL}/rendez_vous/create`, rdv, Token_Header());
};

export const Delete_Rdv = (id) => {
  return axios.delete(`${APIURL}/rendez_vous/${id}`, Token_Header());
};

export const Edit_Rdv = (id, rdv) => {
  return axios.put(`${APIURL}/rendez_vous/${id}`, rdv, Token_Header());
};

export const Get_Rdv = (id) => {
  return axios.get(`${APIURL}/rendez_vous/user/${id}`, Token_Header());
};
