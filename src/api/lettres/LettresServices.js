/** @format */

import axios from 'axios';
import { APIURL, Token_Header } from '../../config/Config';

export const Create_letter = (letter) => {
  return axios.post(`${APIURL}/lettre/create`, letter, Token_Header());
};

export const Delete_letter = (id) => {
  return axios.delete(`${APIURL}/lettre/${id}`, Token_Header());
};

export const Edit_letter = (id, letter) => {
  return axios.put(`${APIURL}/lettre/${id}`, letter, Token_Header());
};

export const Get_letter = (id) => {
  return axios.get(`${APIURL}/lettre/user/${id}`, Token_Header());
};
