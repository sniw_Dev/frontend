/** @format */

import { useState, useEffect } from 'react';
import axios from 'axios';
import { APIURL, Token_Header } from '../config/Config';
export default function useFindUser() {
  const [user, setUser] = useState(null);
  const [isLoading, setLoading] = useState(true);
  useEffect(() => {
    async function findUser() {
      await axios
        .get(`${APIURL}/user`, Token_Header())
        .then((res) => {
          setUser(res.data);
          console.log('first car');
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
        });
    }
    findUser();
  }, []);
  return {
    user,
    isLoading,
  };
}
