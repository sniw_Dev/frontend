/** @format */

import { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { UserContext } from './UserContext';
import { APIURL, Token_Header } from '../config/Config';

export default function useAuth() {
  const navigate = useNavigate();
  const { setUser, user } = useContext(UserContext);
  const [error, setError] = useState(null);
  //set user in context and push them home
  function navigate_service() {
    console.log('navigate_service');
    navigate('/service', { replace: true });
  }

  const setUserContext = async () => {
    return await axios
      .get(`${APIURL}/user`, Token_Header())
      .then((res) => {
        console.log('ici', res.data);
        setUser(res.data);
      })
      .catch((err) => {
        setError(err);
      });
  };
  //register user
  const registerUser = async (data) => {
    return axios.post(`${APIURL}/user/inscription`, data);
  };
  //login user
  const loginUser = async (data) => {
    const { email, password } = data;
    return axios
      .post(
        `${APIURL}/user/authentification`,

        {
          email,
          password,
        },
        { withCredentials: true }
      )
      .then(async (res) => {
        localStorage.setItem('token', res.data.token);
        localStorage.setItem('user', JSON.stringify(res.data.user));
        await setUserContext();
        navigate('/service', { replace: true });
      })
      .catch((err) => {
        alert('email ou mot de passe incorrect');
        setError(err);
      });
  };

  //logout user
  const logoutUser = async () => {
    localStorage.removeItem('token');
    navigate('/', { replace: true });
  };

  return {
    setUserContext,
    registerUser,
    loginUser,
    error,
    logoutUser,
  };
}
