/** @format */

import React, { useState } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Create_letter } from '../api/lettres/LettresServices';
import { ClipLoader } from 'react-spinners';
import { useFormik } from 'formik';

function CreateLetter(props) {
  //fent
  const [isloading, setisloading] = useState(false);
  const [show, setShow] = useState(false);
  const handleClose = () => {
    formik.resetForm();

    setShow(false);
  };
  const handleShow = () => setShow(true);

  const initialValues = {
    code_lettre: '',
    destinataire: '',
    letter_content: '',
    TypesLettres: '',
    code_postal: '',
  };

  const onSubmit = async (values) => {
    setisloading(true);
    Create_letter(generate_letter_Object()).then((res) => {
      props.refreshList();
      setisloading(false);
      formik.resetForm();
      handleClose();
    });
  };

  const validate = (values) => {
    const errors = {};
    if (!values.code_lettre) {
      errors.code_lettre = 'oblgatoire';
    }
    if (!values.destinataire) {
      errors.destinataire = 'oblgatoire';
    }
    if (!values.letter_content) {
      errors.letter_content = 'oblgatoire';
    }
    if (!values.TypesLettres) {
      errors.TypesLettres = 'oblgatoire';
    }
    if (!values.code_postal) {
      errors.code_postal = 'oblgatoire';
    }
    return errors;
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  const generate_letter_Object = () => {
    return {
      code_lettre: formik.values.code_lettre,
      adresse_destinataire: formik.values.destinataire,
      code_postal: formik.values.code_postal,
      type: formik.values.TypesLettres,
      letter_body: formik.values.letter_content,
      user_id: props.user,
    };
  };

  if (isloading === false) {
    return (
      <div className='CRUDlettre'>
        <div className='créer'>
          <Modal show={show} onHide={handleClose} variant='primary'>
            <Modal.Header closeButton>
              <Modal.Title>
                <h5>Créer votre lettre</h5>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form onSubmit={formik.handleSubmit}>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                    alignItems: 'flex-start',
                    flexDirection: 'column',
                    width: '100%',
                  }}>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <label for='n'>Donnéer un code du votre lettre:</label>
                    {formik.touched.code_lettre && formik.errors.code_lettre ? (
                      <label style={{ fontSize: '11px', color: 'red' }}>
                        {formik.errors.code_lettre}
                      </label>
                    ) : null}
                  </div>
                  <input
                    onChange={formik.handleChange}
                    value={formik.values.code_lettre}
                    type='text'
                    id='code_lettre'
                    onBlur={formik.handleBlur}
                    style={{ width: '100%' }}
                  />
                </div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                    alignItems: 'flex-start',
                    flexDirection: 'column',
                    width: '100%',
                  }}>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <label for='n'>Adresse du déstinataire:</label>
                    {formik.touched.destinataire &&
                    formik.errors.destinataire ? (
                      <div>
                        <span style={{ fontSize: '11px', color: 'red' }}>
                          {formik.errors.destinataire}
                        </span>
                      </div>
                    ) : null}
                  </div>
                  <input
                    onChange={formik.handleChange}
                    value={formik.values.destinataire}
                    type='text'
                    id='destinataire'
                    onBlur={formik.handleBlur}
                    style={{ width: '100%' }}
                  />
                </div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                    alignItems: 'flex-start',
                    flexDirection: 'column',
                    width: '100%',
                    marginBottom: '25px',
                  }}>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <label for='n'>Code postale::</label>
                    {formik.touched.code_postal && formik.errors.code_postal ? (
                      <div>
                        <span style={{ fontSize: '11px', color: 'red' }}>
                          {formik.errors.code_postal}
                        </span>
                      </div>
                    ) : null}
                  </div>
                  <input
                    onChange={formik.handleChange}
                    value={formik.values.code_postal}
                    type='text'
                    id='code_postal'
                    onBlur={formik.handleBlur}
                    style={{ width: '100%' }}
                  />
                </div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <label for='n'>Contenu</label>
                  {formik.touched.letter_content &&
                  formik.errors.letter_content ? (
                    <div>
                      <span style={{ fontSize: '11px', color: 'red' }}>
                        {formik.errors.letter_content}
                      </span>
                    </div>
                  ) : null}
                </div>
                <textarea
                  id='letter_content'
                  name='letter_content'
                  rows='15'
                  cols='60'
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.letter_content}>
                  Saisir votre lettre:
                </textarea>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <label for='n'>Type</label>
                  {formik.touched.TypesLettres && formik.errors.TypesLettres ? (
                    <div>
                      <span style={{ fontSize: '11px', color: 'red' }}>
                        {formik.errors.TypesLettres}
                      </span>
                    </div>
                  ) : null}
                </div>
                <Form.Group className='mb-3' id='H'>
                  <Form.Check
                    name='TypesLettres'
                    type='checkbox'
                    label='recommandé'
                    value='recommandé'
                    onBlur={formik.handleBlur}
                    checked={formik.values.TypesLettres.includes('recommandé')}
                    onChange={() =>
                      formik.setFieldValue('TypesLettres', 'recommandé')
                    }
                  />
                </Form.Group>
                <Form.Group className='mb-3' id='F'>
                  <Form.Check
                    type='checkbox'
                    label='Lettre prioritaire'
                    value='prioritaire'
                    onBlur={formik.handleBlur}
                    name='TypesLettres'
                    checked={formik.values.TypesLettres.includes('prioritaire')}
                    onChange={() =>
                      formik.setFieldValue('TypesLettres', 'prioritaire')
                    }
                  />
                </Form.Group>
                <Form.Group className='mb-3' id='F'>
                  <Form.Check
                    type='checkbox'
                    label='Lettre ordinaire'
                    value='ordinaire'
                    onBlur={formik.handleBlur}
                    name='TypesLettres'
                    checked={formik.values.TypesLettres.includes('ordinaire')}
                    onChange={() =>
                      formik.setFieldValue('TypesLettres', 'ordinaire')
                    }
                  />
                </Form.Group>
                <Modal.Footer>
                  <Button
                    disabled={!(formik.isValid && formik.dirty)}
                    style={{ width: '30%' }}
                    variant='primary'
                    type='submit'>
                    Créer
                  </Button>
                  <Link to='/ModeleLettre'>
                    <Button variant='primary'>Chosir un modèle</Button>
                  </Link>
                  <Button
                    variant='secondary'
                    style={{ width: '30%' }}
                    onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Form>
            </Modal.Body>
          </Modal>
          <Button variant='warning' onClick={handleShow}>
            {' '}
            Créer une lettre
          </Button>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        {' '}
        <Modal show={show} onHide={handleClose} variant='primary'>
          <Modal.Header closeButton>
            <Modal.Title>
              <h5>Creation de lettre</h5>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <ClipLoader size={150} color={'#123abc'} loading={isloading} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
export default CreateLetter;
