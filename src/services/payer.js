import  React,{useState} from 'react'
import {Link} from 'react-router-dom'
import {Card, Form, Col, Button, Toast,Row, Modal} from 'react-bootstrap'
import './E-lettre.css';
function Payer() {
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);

    return (
        <article> 
      <section> 
      <h1>Payer votre commande</h1>
      <br></br><br></br> 
      <h3>Choisir un modéle du timbre </h3>

      <div className='timbre'>
        <div className='l1'>
      <Card style={{ width: '14rem' }}>
    <Card.Img variant="top" src="timbres/t2.jpg"alt="t1" /><br></br>
      <Card.Body>
      <Card.Title><input type="checkbox" id="md4"/>2,000 dt</Card.Title>
      </Card.Body>
      </Card>

      <Card style={{ width: '14rem' }}>
     <Card.Img variant="top" src="timbres/t3.jpg"alt="t2" /><br></br>
      <Card.Body>
      <Card.Title><input type="checkbox" id="md4"/>1,,000 dt</Card.Title>
      </Card.Body>
      </Card>

     <Card style={{ width: '14rem' }}>
     <Card.Img variant="top" src="timbres/t4.jpg"alt="t3" /><br></br>
      <Card.Body>
      <Card.Title><input type="checkbox" id="md4"/>2,000 dt</Card.Title>
      </Card.Body>
      </Card>

     <Card style={{ width: '14rem' }}>
     <Card.Img variant="top" src="timbres/t6.jpg"alt="t4" /><br></br>
      <Card.Body>
      <Card.Title><input type="checkbox" id="md4"/>5,000 dt</Card.Title>
      </Card.Body>
      </Card>
      </div>

      <div className='l2'> 
      <Card style={{ width: '14rem' }}>
     <Card.Img variant="top" src="timbres/t7.jpg"alt="t4" /><br></br>
      <Card.Body>
      <Card.Title><input type="checkbox" id="md4"/>2,000 dt</Card.Title>
      </Card.Body>
      </Card>

      <Card style={{ width: '14rem' }}>
     <Card.Img variant="top" src="timbres/t8.jpg"alt="t4" /><br></br>
      <Card.Body>
      <Card.Title><input type="checkbox" id="md4"/>4,000 dt</Card.Title>
      </Card.Body>
      </Card>
      </div>
      
      </div>
      </section>




       <br></br><br></br>
      <section>
        <div className='Fpaiemant'>
        <h3>Les coordonnées du votre carte</h3>
       <Form>
         <Form.Group as={Col} controlId="numcarte">
         <Form.Label>Numéro du votre carte  :</Form.Label>
         <Form.Control />
         </Form.Group>

         <Form.Label htmlFor="inputPassword5">Code du votre carte :</Form.Label>
         <Form.Control
         type="password"
         id="code"
         aria-describedby="passwordHelpBlock"/>
       </Form>
        </div>

        <div className='but'>
        <Link to="/service">  
          <Button  variant="primary">Annuler</Button>
        </Link> 
         

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title>Payer votre commande</Modal.Title>
        </Modal.Header>
        <Modal.Body>Votre commande a bien été enregistrée</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        
        </Modal.Footer>
      </Modal>

      <Button variant="primary" onClick={handleShow}>payer</Button>
        </div>
      </section>

      </article>
     
    );
  }
  
  export default Payer;