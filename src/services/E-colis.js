import  React, {useState} from 'react'
import {Link} from 'react-router-dom'
import {Button, Form, Row, Col} from 'react-bootstrap'
import SupprimerColis from './Supprimercolis'
import ConsulterColis from './consultercolis'
import './colis.css'
function Colis() {
    return (
 <div className='Colis'>
  
  <div className='t'> 
 
   <img src='/BC.jpg' alt="img"/>  
   <h1>Envoyer votre colis</h1>
   </div>

  <br></br><br></br> <br></br><br></br>
  <div className='formulaireColis'>
      <h5>Indiquez les coordonnées du destinataire</h5>
    <Form>
     <Row className="mb-3">
     <Form.Group as={Col} controlId="ndes">
     <Form.Label>Nom du destinataire :</Form.Label>
     <Form.Control />
     </Form.Group>
     <Form.Group as={Col} controlId="pdes">
     <Form.Label>Prénom destinataire:</Form.Label>
     <Form.Control />
     </Form.Group>
     </Row>

     <Row className="mb-3">
     <Form.Group as={Col} controlId="Tel">
     <Form.Label>Numéro du télèphone :</Form.Label>
     <Form.Control />
     </Form.Group>
     <Form.Group as={Col} controlId="adrdes">
     <Form.Label>Adresse ou code postale du destinataire :</Form.Label>
     <Form.Control />
     </Form.Group>
     </Row>
     

     <div className='Ccolis'> 
     <h5>Indiquez les coordonnées du votre colis</h5>

     <Row className="mb-3">
     <Form.Group as={Col} controlId="type">
     <Form.Label>Type du colis :</Form.Label>
     <Form.Control />
     </Form.Group>
     <Form.Group as={Col} controlId="pdes">
     <Form.Label>Le poid du colis :</Form.Label>
     <Form.Control />
     </Form.Group>
     </Row>


     <Form.Group as={Col} controlId="codeColis">
     <Form.Label>Donnéer un code du votre colis :</Form.Label>
     <Form.Control />
     </Form.Group>
     </div>
</Form>
<br></br><br></br>

<div className='CRUDcolis'>
    <Button variant="warning">
        Envoyer
     </Button>
    <div className='supColis'>
      <SupprimerColis/>
    </div>

    <div className='consulterColis'>
      <ConsulterColis/>
    </div>
</div>






</div>
<div className='B'> 
 <Link to="/service">  
   <Button variant="outline-primary">←Retour</Button>
 </Link> 
 <Link to="/payer">   
    <Button variant="outline-primary">Payer→</Button>
 </Link> 
 </div>





</div>
  )
}
export default Colis;