import  React from 'react'
import {Card,Table, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Payer from './payer';
import './E-lettre.css';
function Modéle() {
    return (  
<article>
 <section className='Achat'>

     <h3>Commandez les produits nécessaires pour votre envoi</h3>
     <br></br> <br></br>
     <h5>Des fleurs:</h5>
     <br></br>
     <div className='fleur'>
     <br></br> <br></br>
     
<div className="l1"> 
  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/PivoineBlanche.jpg" /><br></br>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f1"/>Pivoine blanc 30DT</Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/PivoineRouge.jpg"/>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f2"/>Pivoine rouge 25DT</Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/Hortensia.jpg" />
  <Card.Body>
  <Card.Title><input type="checkbox" id="f3"/>Hortensia rose 36DT</Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/HortensiaB.jpg" />
  <Card.Body>
  <Card.Title><input type="checkbox" id="f4"/>Hortensia bleu 60DT</Card.Title>
  </Card.Body>
  </Card>
</div>




 <div className='l2'> 
  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/PivoineRose.jpg"/><br></br>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f5"/>Pivoine rose 40DT</Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/Jacinthe.jpeg" /><br></br>
  <Card.Body><br></br>
  <Card.Title><input type="checkbox" id="f6"/>Jacinthe 66DT </Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/RoseAvalanche.jpg"/>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f7"/>Rose Avalanche 68DT</Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/RoseRouge.jpg"/><br></br>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f8"/>Rose rouge 70DT</Card.Title>
  </Card.Body>
  </Card> 

</div>

<div className='l3'> 
  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/Pivoinebleu.jpg" />
  <Card.Body>
  <Card.Title><input type="checkbox" id="f9"/>Pivoine bleu 55DT</Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/lavande.jpg"/>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f10"/>Lavande 69DT</Card.Title>
  </Card.Body>
  </Card>

  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/rosesorange.jpg"/>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f11"/>Rose  orange 80DT</Card.Title>
  </Card.Body>
  </Card>
  <Card style={{ width: '14rem' }}>
  <Card.Img variant="top" src="/fleurs/Violette.jpg"/>
  <Card.Body>
  <Card.Title><input type="checkbox" id="f12"/>Violette 78DT</Card.Title>
  </Card.Body>
  </Card>
</div>  

    </div>






<br></br> <br></br><br></br><br></br>
<h5>Enveloppes:</h5>
<br></br><br></br>
<div className='envoloppe'>
  <div className='le1'> 
    <Card style={{ width: '12rem' }}>
    <Card.Img variant="top" src="/envoloppe/e1.jpg"/>
    <Card.Body>
    <Card.Title><input type="checkbox" id="env1"/>Enveloppe 1 5,000dt</Card.Title>
    </Card.Body>
    </Card> 

    <Card style={{ width: '12rem' }}>
    <Card.Img variant="top" src="/envoloppe/e4.jpg"/>
    <Card.Body>
    <Card.Title><input type="checkbox" id="env2"/>Enveloppe 2 2,120DT</Card.Title>
    </Card.Body>
    </Card>

    <Card style={{ width: '12rem' }}>
    <Card.Img variant="top" src="/envoloppe/e3.webp"/>
    <Card.Body>
    <Card.Title><input type="checkbox" id="env3"/>Enveloppe 3 2,200DT</Card.Title>
    </Card.Body>
    </Card> 
</div>

<div className='le2'> 
  <Card style={{ width: '12rem' }}>
  <Card.Img variant="top" src="/envoloppe/e10.jpg"/>
  <Card.Body>
  <Card.Title><input type="checkbox" id="env4"/>Enveloppe 4 4,050DT</Card.Title>
  </Card.Body>
  </Card>
    
  <Card style={{ width: '12rem' }}>
  <Card.Img variant="top" src="/envoloppe/e6.jpg" />
  <Card.Body>
  <Card.Title><input type="checkbox" id="env5"/>Enveloppe 6 1,200DT</Card.Title>
  </Card.Body>
  </Card>
   
  <Card style={{ width: '12rem' }}>
  <Card.Img variant="top" src="/envoloppe/e5.jpg"/>
  <Card.Body><br></br>
  <Card.Title><input type="checkbox" id="env6"/>Enveloppe 5 3,100DT</Card.Title>
  </Card.Body>
  </Card> 
</div>



<div className='l3'> 
  <Card style={{ width: '12rem' }}>
  <Card.Img variant="top" src= "/envoloppe/e9.jpg" />
  <Card.Body><br></br>
  <Card.Title><input type="checkbox" id="env7"/>Enveloppe 9 1,500DT</Card.Title>
  </Card.Body>
  </Card> 

  <Card style={{ width: '12rem' }}>
  <Card.Img variant="top" src="/envoloppe/e7.jpg" />
  <Card.Body><br></br>
  <Card.Title><input type="checkbox" id="env8"/>Enveloppe 7 3,500DT</Card.Title>
  </Card.Body>
  </Card> 

  <Card style={{ width: '12rem' }}>
  <Card.Img variant="top" src="/envoloppe/e8.png" />
  <Card.Body>
  <Card.Title><input type="checkbox" id="env9"/>Enveloppe 8 2,530DT</Card.Title>
  </Card.Body>
  </Card> 
</div>

</div>







<br></br> <br></br><br></br><br></br>
    <h5> Embalage des colis:</h5>
    <br></br><br></br>
    <div className='EmbalageColis'>

  <div className='l1'>  
<Card style={{ width: '12rem' }}>
<Card.Img variant="top" src="/embalage/EMB1.jpg"/>
<Card.Body>
<Card.Title><input type="checkbox" id="emb1"/>Embalage 1 8,700DT</Card.Title>
</Card.Body>
</Card>

<Card style={{ width: '12rem' }}>
<Card.Img variant="top" src="/embalage/3EMB.jpg"/>
<Card.Body>
<Card.Title><input type="checkbox" id="emb2"/>Embalage 2 9,400DT</Card.Title>
</Card.Body>
</Card> 

<Card style={{ width: '12rem' }}>
<Card.Img variant="top" src="/embalage/EMB4.jpg"/>
<Card.Body>
<Card.Title><input type="checkbox" id="emb3"/>Embalage 3 8,600DT</Card.Title>
</Card.Body><br></br>
</Card> 
</div>   


<div className='l2'> 
<Card style={{ width: '12rem' }}>
<Card.Img variant="top" src="/embalage/EMB2.jpg"/>
<Card.Body><br></br>
<Card.Title><input type="checkbox" id="emb4"/>Embalage 4 9,170DT</Card.Title>
</Card.Body><br></br>
</Card> 

<Card style={{ width: '12rem' }}>
<Card.Img variant="top" src="/embalage/EMB5.jpg"/>
<Card.Body>
<Card.Title><input type="checkbox" id="emb5"/>Embalage 5 8,690DT</Card.Title>
</Card.Body>
</Card> 

<Card style={{ width: '12rem' }}><br></br>
<Card.Img variant="top" src="/embalage/EMB8.jpg"/>
<Card.Body>
<Card.Title><input type="checkbox" id="emb6"/>Embalage 6 9,010DT</Card.Title>
</Card.Body><br></br><br></br>
</Card> 

</div>


<div className='l3'>
<Card style={{ width: '12rem' }}><br></br>
<Card.Img variant="top" src="/embalage/EMB9.jpg"/>
<Card.Body><br></br>
<Card.Title><input type="checkbox" id="emb7"/>Embalage 7 8,760DT</Card.Title>
</Card.Body>
</Card> 

<Card style={{ width: '12rem' }}><br></br>
<Card.Img variant="top" src="/embalage/EMB7.jpg"/>
<Card.Body>
<Card.Title><input type="checkbox" id="emb8"/>Embalage 8 9,600DT</Card.Title>
</Card.Body>
</Card> 


<Card style={{ width: '12rem' }}>
<Card.Img variant="top" src="/embalage/EMB6.jpg"/><br></br>
<Card.Body>
<Card.Title><input type="checkbox" id="emb9"/>Embalage 9 9,850DT</Card.Title>
</Card.Body>
</Card> 
</div>
</div>
</section>  
  <br></br><br></br>

   <section>
    <div className='Btn'>
     <Link to="/E-lettre">  
       <Button variant="outline-primary">←Annuler</Button>
     </Link> 
     <Link to="/payer">   
      <Button variant="outline-primary">Valider→</Button>
     </Link> 
    </div>
   <br></br>
    </section>
</article>  
    )
}

export default Modéle;