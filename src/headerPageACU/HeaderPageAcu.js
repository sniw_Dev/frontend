/** @format */

// import React from 'react'
// import { Carousel } from 'react-bootstrap';
// import Cnx from '../connection/cnx';
// import Nav from '../NavBar/nav';
// function HeaderPageAcu(){
//     return(
//         <header>

//   <Carousel variant="dark">
//     <Carousel.Item>
//     <img className="d-block w-100" src="/lettreENligne.jpg"alt="First slide"/>
//     </Carousel.Item>

//    <Carousel.Item>
//    <img className="d-block w-100"src="/coliEnligne.jpg" alt="Second slide"/>
//    </Carousel.Item>

//    <Carousel.Item>
//    <img className="d-block w-100"src="/rendezVous.jpg"alt="Third slide"/>
//    </Carousel.Item>
//  </Carousel>

// <div className='navbar'>
//     <Nav/>
// </div>
// <br></br><br></br><br></br><br></br>
//  <div className='contenuPA'>
//    <Cnx/>
//  </div>
//        </header>
//     )
// }
// export default HeaderPageAcu;
import React, { useState, useEffect } from 'react';
import { useContext } from 'react';

import Button from '@mui/material/Button';
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import PersonIcon from '@mui/icons-material/Person';
import Cnx from '../connection/cnx';
import './headerPage.css';
import Fade from '@mui/material/Fade';
import Inscrit from '../NavBar/inscrit';
import { Navigate } from 'react-router-dom';
import { UserContext } from '../hooks/UserContext';
import { useNavigate } from 'react-router-dom';

const HeaderPageAcu = () => {
  const navigate = useNavigate();

  const [infosection, setinfoSection] = useState(true);
  const [loginSection, setloginSection] = useState(false);
  const { user } = useContext(UserContext);
  // if (user) {
  //   console.log('m here');
  //   <Navigate to='/service' />;
  // }

  useEffect(() => {}, [infosection, loginSection]);
  useEffect(() => {
    console.log(user);

    if (user) {
      console.log('blablabal');
      navigate('/service', { replace: true });
    }
  }, [user]);

  const handleloginSection = () => {
    setloginSection(true);
    setinfoSection(false);
  };

  const handleinfoSection = () => {
    setinfoSection(true);
    setloginSection(false);
  };

  return (
    <>
      <div className='landing-page-main'>
        {infosection && (
          <Fade in={infosection}>
            <div className='main_text_container'>
              <img src='/logo_poste.png' alt='logo' className='logo' />
              <p style={{ fontSize: '40px' }}>La Poste Encore plus proche </p>
              <p>
                Myposte est la plateforme digitale disponible via web destinée à
                sa clientèle postale Particuliers et permet un ensemble de
                services en ligne.
              </p>
              <div className='action_section'>
                <Inscrit />
                <Button
                  style={{
                    backgroundColor: '#FEBF17',
                    borderRadius: '10px',
                    fontStyle: 'bold',
                    fontSize: '20px',
                  }}
                  onClick={() => handleloginSection()}
                  variant='contained'
                  startIcon={<PersonIcon />}>
                  Se connecter
                </Button>
              </div>
            </div>
          </Fade>
        )}

        {loginSection && <Cnx retour={handleinfoSection} />}

        <div className='landing-page-header-image'>
          <img src='/6.png' alt='logo' />
        </div>
      </div>
    </>
  );
};

export default HeaderPageAcu;
