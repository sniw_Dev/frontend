/** @format */

import React, { useEffect, useState } from 'react';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import { ClipLoader } from 'react-spinners';
import { UserContext } from '../hooks/UserContext';
import { useContext } from 'react';
import Button from '@mui/material/Button';
import Inventory2Icon from '@mui/icons-material/Inventory2';
import {
  Create_colis,
  Edit_colis,
  Get_colis,
} from '../api/colis/ColisService.js';
import ColisCard from './ColisCard';
import './Colis.css';
import { useFormik } from 'formik';

const Colis = (props) => {
  const { user } = useContext(UserContext);
  const [activeColis, setActiveColis] = useState(null);
  const [ListColis, setListColis] = useState([]);
  const [isloading, setIsloading] = useState(false);
  const [Modifier, setModifier] = useState(false);

  const initialValues = {
    nom_destinataire: '',
    adresse_destinataire: '',
    prenom_destinataire: '',
    type: '',
    poid_colis: '',
    code_colis: '',
    numero_destinataire: '',
    user_id: user._id,
  };

  const validate = (values) => {
    const errors = {};

    if (!values.nom_destinataire) {
      errors.nom_destinataire = 'oblgatoire';
    }
    if (!values.adresse_destinataire) {
      errors.adresse_destinataire = 'oblgatoire';
    }

    if (!values.prenom_destinataire) {
      errors.prenom_destinataire = 'oblgatoire';
    }

    if (!values.type) {
      errors.type = 'oblgatoire';
    }
    if (!values.poid_colis) {
      errors.poid_colis = 'oblgatoire';
    }
    if (!values.code_colis) {
      errors.code_colis = 'oblgatoire';
    }

    if (!values.numero_destinataire) {
      errors.numero_destinataire = 'oblgatoire';
    }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validate,
  });
  console.log(formik.touched);

  useEffect(() => {
    formik.setTouched({}, false);
    refreshList();
  }, []);

  useEffect(() => {
    console.log(ListColis, 'updated');
  }, [ListColis, activeColis, Modifier]);

  const handleAjouter = () => {
    setIsloading(true);

    Create_colis(formik.values).then((res) => {
      setIsloading(false);
      formik.resetForm();
      refreshList();
    });
  };

  const handleModifier = () => {
    setIsloading(true);

    Edit_colis(activeColis._id, formik.values).then((res) => {
      setIsloading(false);
      setModifier(false);
      refreshList();
      formik.resetForm();
    });
  };

  const SwitchToModifier = () => {
    setModifier(true);
    formik.setFieldValue('nom_destinataire', activeColis.nom_destinataire);
    formik.setFieldValue(
      'adresse_destinataire',
      activeColis.adresse_destinataire
    );
    console.log(
      'prenom',
      'prenom_destinataire',
      activeColis.prenom_destinataire
    );
    formik.setFieldValue(
      'prenom_destinataire',
      activeColis.prenom_destinataire
    );
    formik.setFieldValue('type', activeColis.type);
    formik.setFieldValue('poid_colis', activeColis.poid_colis);
    formik.setFieldValue('code_colis', activeColis.code_colis);
    formik.setFieldValue(
      'numero_destinataire',
      activeColis.numero_destinataire
    );
  };

  const refreshList = () => {
    Get_colis(user._id).then((res) => {
      const newFeatures = res.data;

      console.log(res.data, 'Colis');
      setListColis(newFeatures);
    });
  };

  const CreateFormColis = () => {
    return (
      <Paper
        elevation={3}
        style={{
          height: 'fit-content',
          padding: '10px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          position: 'sticky',
        }}>
        {isloading ? (
          <div
            style={{
              marginTop: '30px',
              width: '100%',
              height: '100vh',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ClipLoader size={150} color={'#123abc'} loading={isloading} />
          </div>
        ) : (
          <>
            <div className='iconholder'>
              <Inventory2Icon style={{ fontSize: '100px', color: '#333333' }} />
            </div>
            <form>
              {formik.touched.nom_destinataire &&
              formik.errors.nom_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.nom_destinataire}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='nom_destinataire'
                label=' Nom du destinataire :'
                fullWidth
                variant='outlined'
                value={formik.values.nom_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.prenom_destinataire &&
              formik.errors.prenom_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.prenom_destinataire}
                  </span>
                </div>
              ) : null}

              <TextField
                autoFocus
                margin='dense'
                name='prenom_destinataire'
                label='Prénom destinataire:'
                fullWidth
                variant='outlined'
                value={formik.values.prenom_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.numero_destinataire &&
              formik.errors.numero_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.numero_destinataire}
                  </span>
                </div>
              ) : null}

              <TextField
                autoFocus
                margin='dense'
                id='numero_destinataire'
                label='Numéro du télèphone :'
                fullWidth
                name='numero_destinataire'
                variant='outlined'
                value={formik.values.numero_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              {formik.touched.adresse_destinataire &&
              formik.errors.adresse_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.adresse_destinataire}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='adresse_destinataire'
                name='adresse_destinataire'
                label='addresse destinataire :'
                fullWidth
                variant='outlined'
                value={formik.values.adresse_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              {formik.touched.type && formik.errors.type ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.type}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='type'
                label='Type du colis :'
                fullWidth
                variant='outlined'
                value={formik.values.type}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.poid_colis && formik.errors.poid_colis ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.poid_colis}
                  </span>
                </div>
              ) : null}

              <TextField
                autoFocus
                margin='dense'
                id='poid_colis'
                label='Le poid du colis :'
                fullWidth
                variant='outlined'
                value={formik.values.poid_colis}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.code_colis && formik.errors.code_colis ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.code_colis}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='code_colis'
                label=' code du  colis :'
                fullWidth
                variant='outlined'
                value={formik.values.code_colis}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              <Button
                fullWidth
                variant='outlined'
                type='submit'
                disabled={!(formik.isValid && formik.dirty)}
                onClick={handleAjouter}>
                Ajouter
              </Button>
            </form>
          </>
        )}
      </Paper>
    );
  };
  const updateFormColis = () => {
    return (
      <Paper
        elevation={3}
        style={{
          height: 'fit-content',
          padding: '10px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          position: 'sticky',
        }}>
        {isloading ? (
          <div
            style={{
              marginTop: '30px',
              width: '100%',
              height: '100vh',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ClipLoader size={150} color={'#123abc'} loading={isloading} />
          </div>
        ) : (
          <>
            <div className='iconholder'>
              <Inventory2Icon style={{ fontSize: '100px', color: '#333333' }} />
            </div>
            <form>
              {formik.touched.nom_destinataire &&
              formik.errors.nom_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.nom_destinataire}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='nom_destinataire'
                label=' Nom du destinataire :'
                fullWidth
                variant='outlined'
                value={formik.values.nom_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.prenom_destinataire &&
              formik.errors.prenom_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.prenom_destinataire}
                  </span>
                </div>
              ) : null}

              <TextField
                autoFocus
                margin='dense'
                name='prenom_destinataire'
                label='Prénom destinataire:'
                fullWidth
                variant='outlined'
                value={formik.values.prenom_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.numero_destinataire &&
              formik.errors.numero_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.numero_destinataire}
                  </span>
                </div>
              ) : null}

              <TextField
                autoFocus
                margin='dense'
                id='numero_destinataire'
                label='Numéro du télèphone :'
                fullWidth
                name='numero_destinataire'
                variant='outlined'
                value={formik.values.numero_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              {formik.touched.adresse_destinataire &&
              formik.errors.adresse_destinataire ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.adresse_destinataire}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='adresse_destinataire'
                name='adresse_destinataire'
                label='addresse destinataire :'
                fullWidth
                variant='outlined'
                value={formik.values.adresse_destinataire}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              {formik.touched.type && formik.errors.type ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.type}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='type'
                label='Type du colis :'
                fullWidth
                variant='outlined'
                value={formik.values.type}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.poid_colis && formik.errors.poid_colis ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.poid_colis}
                  </span>
                </div>
              ) : null}

              <TextField
                autoFocus
                margin='dense'
                id='poid_colis'
                label='Le poid du colis :'
                fullWidth
                variant='outlined'
                value={formik.values.poid_colis}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />

              {formik.touched.code_colis && formik.errors.code_colis ? (
                <div>
                  <span style={{ fontSize: '11px', color: 'red' }}>
                    {formik.errors.code_colis}
                  </span>
                </div>
              ) : null}
              <TextField
                autoFocus
                margin='dense'
                id='code_colis'
                label=' code du  colis :'
                fullWidth
                variant='outlined'
                value={formik.values.code_colis}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              <div style={{ display: 'flex' }}>
                <Button
                  fullWidth
                  variant='outlined'
                  onClick={() => {
                    setModifier(false);
                  }}>
                  Retour
                </Button>
                <Button fullWidth variant='outlined' onClick={handleModifier}>
                  Modifier
                </Button>
              </div>
            </form>
          </>
        )}
      </Paper>
    );
  };

  const updateActiveColis = (colis) => {
    setActiveColis(colis);
  };

  return (
    <Grid container spacing={3} xs={12} sm={12} md={12}>
      <Grid
        item
        xs={12}
        sm={3}
        md={3}
        style={{ position: 'sticky', marginRight: '15px' }}>
        {Modifier ? updateFormColis() : CreateFormColis()}
      </Grid>
      <Grid
        container
        style={{
          display: 'flex',
          alignItems: 'flex-start',
          marginTop: '10px',
        }}
        spacing={3}
        xs={12}
        sm={9}
        md={9}>
        {ListColis &&
          ListColis.map((colis, index) => {
            return (
              <Grid item sm={3} md={3}>
                <ColisCard
                  colis={colis}
                  switchtoupdate={SwitchToModifier}
                  updateActiveColis={updateActiveColis}
                  handleupdate={refreshList}
                />
              </Grid>
            );
          })}
      </Grid>
    </Grid>
  );
};
export default Colis;
