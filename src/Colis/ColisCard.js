/** @format */

import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { Delete_colis } from '../api/colis/ColisService';
const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function ColisCard(props) {
  const MySwal = withReactContent(Swal);

  const [expanded, setExpanded] = React.useState(false);
  const [colis, setColis] = useState(props.colis);

  useEffect(() => {
    console.log(colis, 'colis');
  }, [colis]);

  useEffect(() => {
    console.log(props.colis, 'props.colis');
    setColis(props.colis);
  }, [props.colis]);

  const handleupdate = () => {
    props.updateActiveColis(colis);
    props.switchtoupdate();
  };

  const handleDelete = () => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        Delete_colis(props.colis._id).then((res) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');

          props.handleupdate();
        });
      }
    });
  };

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label='recipe'>
            {colis.prenom_destinataire[0]}
          </Avatar>
        }
        action={
          <IconButton aria-label='settings'>
            <MoreVertIcon />
          </IconButton>
        }
        title={colis.nom_destinataire + ' ' + colis.prenom_destinataire}
        subheader={colis.numero_destinataire}
      />
      <CardMedia
        component='img'
        height='194'
        image='https://thumbs.dreamstime.com/b/style-de-dessin-anim%C3%A9-d-ic%C3%B4ne-colis-entrep%C3%B4t-l-du-vecteur-pour-la-conception-sites-web-isol%C3%A9-sur-le-fond-blanc-198338906.jpg'
        alt='Paella dish'
      />
      <CardContent>
        <Typography variant='body2' color='text.secondary'>
          <div>{'adresse destinataire : ' + colis.adresse_destinataire}</div>
          <div>{'code colis : ' + colis.code_colis}</div>
          <div>{'Type du colis : ' + colis.type}</div>
          <div>{'Poids  colis : ' + colis.poid_colis + ' kg'}</div>
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton onClick={handleupdate} aria-label='add to favorites'>
          <EditIcon />
        </IconButton>
        <IconButton
          aria-label='share'
          onClick={() => {
            handleDelete();
          }}>
          <DeleteIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
}
